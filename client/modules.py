import json, requests, hashlib, os
from rsa import *

def addEntry(entry):
    # Check if it already have been added
    hash = hashlib.sha1(str(entry["nkey"]).encode("utf-8")).hexdigest()
    if "%s.json" % hash in os.listdir("feeds"):
        f = open(f"feeds/{hash}.json", "r").read()
        f = json.loads(f)
        if entry in f:
            return "ALREADY ADDED"

    # Check if the file is valid
    if valid(
        entry["author"] + entry["content"],
        entry["signature"],
        entry["ekey"],
        entry["nkey"],
    ):
        try:
            # Get the feed
            newFeed = json.loads(open(f"feeds/{hash}.json", "r").read())
            newFeed.append(entry)

        # If file not found, create one
        except FileNotFoundError:
            # Create a feed
            newFeed = []
            newFeed.append(entry)
        open(f"feeds/{hash}.json", "w").write(json.dumps(newFeed))
        return "VALID"
    else:
        return "INVALID"
