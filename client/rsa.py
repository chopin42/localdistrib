from Crypto.PublicKey import RSA
from hashlib import sha512


def getKeys():
    keyPair = RSA.generate(bits=1024)
    return keyPair


def sign(msg, dkey, nkey):
    if type(dkey) == type("string"):
        dkey = int(dkey, 0)
    if type(nkey) == type("string"):
        nkey = int(nkey, 0)

    msg = bytes(msg, "utf-8")
    hash = int.from_bytes(sha512(msg).digest(), byteorder="big")
    signature = pow(hash, dkey, nkey)
    return signature


def valid(msg, signature, ekey, nkey):
    if type(signature) == type("string"):
        signature = int(signature, 0)
    if type(ekey) == type("string"):
        ekey = int(ekey, 0)
    if type(nkey) == type("string"):
        nkey = int(nkey, 0)

    msg = bytes(msg, "utf-8")
    hash = int.from_bytes(sha512(msg).digest(), byteorder="big")
    hashFromSignature = pow(signature, ekey, nkey)
    if hash == hashFromSignature:
        return True
    else:
        return False
