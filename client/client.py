import requests, json, os
from modules import addEntry
from rsa import *

libraries = open("libraries.json", "r").read()
libraries = json.loads(libraries)

# Get the keys
try:
    keys = json.loads(open("keys.json", "r").read())
except FileNotFoundError:
    keys = getKeys()
    keys = {
        "n": keys.n,
        "d": keys.d,
        "e": keys.e,
    }
    open("keys.json", "w").write(json.dumps(keys))
nkey = keys['n']
dkey = keys['d']
ekey = keys['e']

print("""
You can apply the following commands:

- connect: Connect to a remote host
- post: To post a new content and sending it to all your hosts
- follow: Follow another user you have the id
- download_all: fetch all the data of all your hosts (CAN TAKE A LOT OF SPACE)
- display: Display the messages from all the feeds
- discover: Get all the users's id of all your hosts
""")

i = input()

if i == "connect":
    library = input("Host> ")

    try:
        r = requests.get(library)
        print("The server is available.")
    except:
        print(f"{libeary} cannot be reached! Abort.")
        exit()

    if library in libraries:
        print("Library already exists.")
    else:
        libraries.append(library)
        open("libraries.json", "w").write(json.dumps(libraries))

    # Make one masterfeed
    everything = []
    for feed in os.listdir("feeds"):
        feed = open(f"feeds/{feed}", "r").read()
        feed = json.loads(feed)

        for post in feed:
            everything.append(post)

    # Send the list everything to all the libraries
    for library in libraries:
        library = library + "/post/feed"
        try:
            r = requests.post(library, data=json.dumps(everything))
        except:
            print(f"{library} cannot be reached. Skip.")
        print(r.status_code)
    print("The request has been sent to all libraries.")

elif i == "post":
    author = input("Author> ")
    content = input("Content> ")

    # Sign the post
    signature = sign(
        author + content,
        dkey,
        nkey,
    )

    # Make the new entry
    entry = {
        "author": author,
        "content": content,
        "signature": signature,
        "nkey": nkey,
        "ekey": ekey,
    }

    # Add the entry to the feed
    result = addEntry(entry)
    print(result)

    # Send the entry to the libraries
    for library in libraries:
        try:
            library = library + "/post/entry"
            r = requests.post(library, data=json.dumps(entry))
            print(r.status_code)
        except:
            print(f"{library} cannot be reached.")

elif i == "follow":
    user = input("User> ")

    # Request to the libraries
    for library in libraries:
        library = library + "/get/user/" + user

        try:
            r = requests.get(library)
            if r.text != "FALSE":
                newFeed = json.loads(r.text)
                for post in newFeed:
                    addEntry(post)
                print(f"The feed of {user} has been added.")
            else:
                print("This user doesn't exist on the server.")
        except:
            pass
            
elif i == "download_all":
    input("Press ENTER to continue or CTRL+C to cancel.")
    for library in libraries:
        try:
            library = library + "/get/everything"
            r = requests.get(library)
            feed = json.loads(r.text)
            for entry in feed:
                addEntry(entry)
        except:
            print(f"{library} cannot be reached.")
    print("All the data from all the libraries has been downloaded!")

elif i == "display":
    # Fetch the data with the hosts
    for feed in os.listdir("feeds"):
        for library in libraries:
            library = library + "/get/user/" + feed.split(".json")[0]
            
            try:
                r = requests.get(library)
                if r.text != "FALSE":
                    newFeed = json.loads(r.text)
                    for post in newFeed:
                        addEntry(post)
            except:
                pass

    everything = []
    for feed in os.listdir("feeds"):
        feed = open(f"feeds/{feed}", "r").read()
        feed = json.loads(feed)

        for entry in feed:
            everything.append(entry)

    for entry in everything:
        print(f"{entry['author']} : {entry['content']}")

elif i == "discover":
    for library in libraries:
        try:
            library = library + "/discover"
            r = requests.get(library)
            users = json.loads(r.text)
        

            for user in users:
                print(f"{user['nickname']} : {user['id']}")
        except:
            print(f"{library} cannot be reached.")
