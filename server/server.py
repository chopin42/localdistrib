from flask import Flask, request
import json, requests, hashlib, os
from rsa import *

app = Flask(__name__)


def addEntry(entry):
    # Check if it already have been added
    hash = hashlib.sha1(str(entry["nkey"]).encode("utf-8")).hexdigest()
    if "%s.json" % hash in os.listdir("feeds"):
        f = open(f"feeds/{hash}.json", "r").read()
        f = json.loads(f)
        if entry in f:
            return "ALREADY ADDED"

    # Check if the file is valid
    if valid(
        entry["author"] + entry["content"],
        entry["signature"],
        entry["ekey"],
        entry["nkey"],
    ):
        try:
            # Get the feed
            newFeed = json.loads(open(f"feeds/{hash}.json", "r").read())
            newFeed.append(entry)

        # If file not found, create one
        except FileNotFoundError:
            # Create a feed
            newFeed = []
            newFeed.append(entry)
        open(f"feeds/{hash}.json", "w").write(json.dumps(newFeed))
        return "VALID"
    else:
        return "INVALID"


# Add one entry
@app.route("/post/entry", methods=["GET", "POST"])
def postEntry():
    entry = json.loads(request.get_data())

    result = addEntry(entry)
    return result


# Add a complete feed
@app.route("/post/feed", methods=["GET", "POST"])
def postFeed():
    newFeed = json.loads(request.get_data())

    for entry in newFeed:
        addEntry(entry)

    return "OK"


# Return a feed after follow request
@app.route("/get/user/<user>")
def getUser(user):
    #r = json.loads(request.get_data())
    r = user

    feeds = os.listdir("feeds")
    if f"{r}.json" in feeds:
        f = open(f"feeds/{r}.json", "r").read()
        return f
    else:
        return "FALSE"


@app.route("/get/everything")
def getEverything():
    everything = []

    feeds = os.listdir("feeds")
    for feed in feeds:
        feed = open(f"feeds/{feed}", "r").read()
        feed = json.loads(feed)
        for post in feed:
            everything.append(post)

    return json.dumps(everything)

@app.route("/discover")
def who():
    users = []

    feeds = os.listdir("feeds")
    for feedId in feeds:
        feed = open(f"feeds/{feedId}", "r").read()
        feed = json.loads(feed)
        for post in feed:
            user = {
                "nickname": post['author'],
                "id": feedId.split('.json')[0]
            }
            if user not in users:
                users.append(user)
    return json.dumps(users)

app.run(
    host='0.0.0.0',
    debug=True
)
