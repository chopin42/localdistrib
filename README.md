# LocalDistrib

`LocalDistrib` is a micro-blogging application to communicate using a new way.

You can communicate in 100% peer-to-peer

```sequence
Bob -> Alice: This is my feed
Alice -> Alice: Adds the feed
Alice -> Bob: This is my feed
Bob -> Bob: Adds the feed
```

Or, if you can't get a physical accesss you can use a *library server* to store **all** the data.

```sequence
Bob -> Library: This is my feed
Library -> Library: Adds the feed
Library -> Bob: This is my feed
Bob -> Bob: Adds the feed
```

Or you can use the Library to store the data for you.

```sequence
Bob -> Library: This is my feed
Library -> Library: Adds the feed
Bob -> Library: Give me the feed of Alice
Library -> Bob: This is the feed
Bob -> Bob: Adds the feed of Alice
```

By this way the app is both decentralized and distributed. You don't need to use the servers, it's just more convinient. These servers are making the difference between group chat and social network.

## Installation

To install this follow these instructions:

```shell
python3 app.py
```

## Usage

Run `WHAT TO RUN`

```shell
HOW TO USE HERE
```

## Contribute

To get all the informations about contributions, please see the [CONTRIBUTING file](./CONTRIBUTING.md)

## License

This project is under GNU-GPL-v3. You can find more informations in the [LICENSE file](./LICENSE).
